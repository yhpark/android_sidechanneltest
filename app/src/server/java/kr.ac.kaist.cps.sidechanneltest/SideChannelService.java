package kr.ac.kaist.cps.sidechanneltest;

import android.app.Service;
import android.content.Intent;
import android.net.LocalServerSocket;
import android.net.LocalSocket;
import android.os.IBinder;
import android.os.MemoryFile;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Random;

/**
 * Created by yhpark on 11/25/2015.
 */
public class SideChannelService extends Service {
    private static final String TAG = "SideChannelService";

    private static final String SOCKET_ADDR = "test";
    private static final String MF_NAME = "test_mf";

    private class LocalServerThread extends Thread {
        private LocalServerSocket localServerSocket;
        private LocalSocket localSocket;

        @Override
        public void run() {
            try {
                Log.i(TAG, "Initializing server socket");
                localServerSocket = new LocalServerSocket(SOCKET_ADDR);
            } catch (Exception e) {
                Log.e(TAG, "Failed to create server socket", e);
                return;
            }

            try {
                Log.i(TAG, "Accepting from server");
                localSocket = localServerSocket.accept();
            } catch (Exception e) {
                Log.e(TAG, "Failed to create server socket", e);
                return;
            }

            try {
                Log.i(TAG, "Accepted, reading data");
                InputStream is = localSocket.getInputStream();

                int data;
                while ((data = is.read()) != -1)
                    Log.i(TAG, "Byte received: " + data);
            } catch (IOException e) {
                Log.e(TAG, "socket read exception", e);
            }
        }
    }

    private LocalServerThread localServerThread;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        localServerThread = new LocalServerThread();
        localServerThread.start();

        try {
            MemoryFile mf = new MemoryFile(MF_NAME, 128);
            byte[] buf = new byte[128];
            Random rand = new Random();
            rand.nextBytes(buf);
            mf.writeBytes(buf, 0, 0, 128);
            Log.i(TAG, "written 128 bytes to MemoryFile");

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 128; i++)
                sb.append(String.valueOf(buf[i]) + " ");
            Log.i(TAG, sb.toString());
        } catch (IOException e) {
            Log.e(TAG, "Memory file creation failed");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        localServerThread.interrupt();
    }
}
