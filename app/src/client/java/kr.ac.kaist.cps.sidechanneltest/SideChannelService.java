package kr.ac.kaist.cps.sidechanneltest;

import android.app.Service;
import android.content.Intent;
import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.os.IBinder;
import android.os.MemoryFile;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by yhpark on 11/25/2015.
 */
public class SideChannelService extends Service {
    private static final String TAG = "SideChannelService";

    private static final String SOCKET_ADDR = "test";
    private static final String MF_NAME = "test_mf";

    private class LocalClientThread extends Thread {
        private LocalSocket localSocket;

        @Override
        public void run() {
            try {
                Log.i(TAG, "Connecting to server");
                localSocket = new LocalSocket();
                localSocket.connect(new LocalSocketAddress(SOCKET_ADDR));
            } catch (Exception e) {
                Log.e(TAG, "Failed to create server socket", e);
                return;
            }

            try {
                Log.i(TAG, "Connected, writing data");
                OutputStream os = localSocket.getOutputStream();

                byte i = 0;
                while (true) {
                    os.write(i);
                    Log.i(TAG, "sent: " + i);
                    i++;
                    sleep(1000);
                }
            } catch (IOException e) {
                Log.e(TAG, "socket read exception", e);
            } catch (InterruptedException e) {
                Log.e(TAG, "interrupted", e);
            }
        }
    }

    private LocalClientThread localClientThread;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        localClientThread = new LocalClientThread();
        localClientThread.start();

        try {
            MemoryFile mf = new MemoryFile(MF_NAME, 128);
            byte[] buf = new byte[128];
            mf.readBytes(buf, 0, 0, 128);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 128; i++)
                sb.append(String.valueOf(buf[i]) + " ");
            Log.i(TAG, sb.toString());
        } catch (IOException e) {
            Log.e(TAG, "Memory file creation failed");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        localClientThread.interrupt();
    }
}
